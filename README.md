GENERAL

This repository contains the pitch data and syllable segmentation from the TIMIT database.

CONTENT DESCRIPTION

The data are provided separately for each of the 6,300 files of the TIMIT database in a MATLAB file format, which can be read with MATLAB or PYTHON.

The data is a structure named "syll", whose field are: 

1) label: phonetic transcription corresponding to each syllable  (1xN)

2) time: syllable start and end times corresponding to each syllable (2xN)

3) f0: pitch values corresponding to each syllable (50XN, see our paper for details)

The data are shared under Creative Commons Attribution-NonCommercial 2.0 licence. All use of these data must reference to the aftermentionned paper:

"N. Obin, J. Beliao (2018). Sparse Coding of Pitch Contours with Deep Auto-Encoders, Speech Prosody, Poznan, Poland, 2018."